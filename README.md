## Asignatura: Informática Industrial y Comunicaciones

Repositorio de código para las prácticas de la asignatura "Informática Industrial y Comunicaciones".

---

## Enlace a Moodle

Moodle de la asignatura: [enlace](http://www.elai.upm.es/moodle/course/view.php?id=59).

## Documentación OpenGL

[API OpenGL](https://www.opengl.org/resources/libraries/glut/spec3/node1.html).

## Pasos para crear un proyecto OpenGL en Visual Studio 2017

* Nuevo proyecto > Visual C++ > Proyecto vacío: Se elige el nombre del proyecto y la ubicación.

* Botón derecho sobre el proyecto > Agregar nuevo elemento: Se elige el nombre del fichero .cpp

* En la carpeta del proyecto se crean dos carpetas:
    * lib/ con la librería glut32.lib
    * include/ con el fichero glut.h

* Lo siguiente es configurar el proyecto para que compile y ejecute con las anteriores librerías. Para ello, botón derecho sobre el proyecto > Propiedades

En Propiedades de configuración > C/C++ > General hay una opción que pone **Directorios de inclusión adicionales**: Hay que editarlo y seleccionar la carpeta **include**.

En Propiedades de configuración > Vinculador > General hay una opción que pone **Directorios de bibliotecas adicionales**: Hay que editarlo y añadir la carpeta **lib**.

En Propiedades de configuración > Vinculador > Entrada hay una opción que pone **Dependencias adicionales**: Hay que editarlo y añadir la librería **glut32.lib**.

* Para probarlo, se puede coger el código del tema 2 inicial y pegarlo en el fichero creado.

* Compila y ejecuta. Tendrás que copiar la librería **glut32.dll** en la carpeta que genera el ejecutable. Si no sabes cuál es, míralo en Propiedades de configuración > General: Es la opción Archivo de salida (en la consola aparece la ruta donde lo genera).
